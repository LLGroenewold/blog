---
title:  Derde week van het project

date: 2018-09-28
---

## Dinsdag 25 september,

**Studio**

We zijn vandaag weer verder gaan werken aan het poster.  Er moest namelijk nog veel gebeuren.

**Vijfde interaction Design LAB**

Tijdens het vijfde LAB heb ik geleerd wat het verschil is tussen een customer journey en een user journey. Een customer journey maakt ook gebruik van de scenario’s

**Animatie keuze vak**

Vandaag hebben geleerd wat typomotion is. Het is animeren met typografie. Er werden weer wat voorbeelden getoond van de animatie stijl. Het was zeer interessant om te zien.

## Vrijdag 28 september,

**Zesde interaction design LAB**

Tijdens het zesde LAB heb geleerd wat een service blueprint is. Het is net als een customer journey maar uitgebreider. Je gaat dan ook in op wat er achter schermen gebeurd bij het bedrijf waarvoor de service blueprint voor bedoeld is.

**Studio**

Vandaag moest de onderzoeksposter worden ingeleverd. Er ging een heleboel mis met het uitprinten op A1 formaat. In het Stadslab was de A1 defect gegaan. We hadden toen besloten hem op 4 A3 vellen uit te printen en vervolgens aan elkaar te plakken.

Voor de poster had ik helaas niks kunnen maken. Ik had afgesproken dat ik de persona zou maken maar door tijdsgebrek hadden we de doelgroep niet meer kunnen spreken waardoor ik dus niks heb kunnen maken voor de persona. 


