﻿
---

title:  Validatie en workshop

date: 2018-02-22

---



## Tweede week van Design Challenge 3


Deze week moest de plan van aanpak gevalideerd worden. Dit heb ik samen gedaan met Leila en Nikola. Jenna moest ergens anders zijn vandaag omdat ze een afspraak had dus hebben we de validatie maar met z'n drieën gedaan. Onze plan van aanpak voldeed niet helemaal aan de eisen waaraan het moest voldoen. (zie validatie formulier hier onder voor meer daar over). Nadat Leila een foto had genomen van het validatie formulier wisten wel meteen waaraan we moesten gaan werken. We hadden het validatie formulier als een checklist gebruikt om zo te zien wat we er nog in moesten zetten.

Woensdag ben ik terug gegaan naar mijn oude team, omdat we moesten gaan werken aan onze presentatie voor in het Paard. We waren uitgekozen voor het beste concept en dat moesten we gaan samenvoegen met een andere team genaamd *"Team Claproses"*. We waren bij elkaar gaan zitten en zijn in overleg gegaan om te kijken wat we zouden gaan doen om onze concepten samen te voegen. In de tussen tijd waren Leila, Nikola en Jenna onze plan van aanpak nog een keer te laten valideren. Dit keer was het wel goed gevalideerd, zie het tweede formulier hieronder.

Later in de middag had ik een workshop over de lifestyle dairy. Voordat ik deze workshop was gaan doen had ik al zelf een lifestyle dairy gemaakt zonder dat ik wist waar het overging en wat je er precies voor moest doen.  (zie foto hieronder). Maar nadat ik de workshop had gevolgd wist ik er wel meer over ga ik er nog eentje maken in de voorjaarsvakantie omdat daarna het validatie moment is. Bij de workshop moest ik mijn gehele tas leeg halen en alle spullen op tafel zetten (zie foto hier onder). Dit was een proef om zo ook een beetje inzicht te krijgen over hoe een persoon. Dit kan je ook zien met een lifestyle dairy.  Zo ben ik meer te weten gekomen over hoe je kritisch moet kijken met dit soort onderzoeken. 

**Afbeeldingen:**

Plan van aanpak validatie formulier 1:

![](Validatie plan van aanpak versie 1.jpg)

Plan van aanpak validatie formulier 2:

![](Validatie plan van aanpak versie 2.jpeg)

Lifestyle Dairy versie 1 foto:

![](lifestyle dairy versie 1 foto.JPG)

Workshop lifestyle dairy foto:

![](Workshop lifestyle dairy foto.JPG)