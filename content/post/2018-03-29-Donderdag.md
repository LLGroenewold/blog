﻿
---

title: Laatste week van DC3
date: 2018-03-29
 
---


**Paper Prototype**


Aan de hand van een overleg over wat mijn groepje en ik hebben gehad afgelopen woensdag over wat we precies gaan doen voor een prototype, ben ik zaterdag aan de slag gegaan met het maken van een prototype op een A3 vel. We besloten voor een app te gaan die simpel is en niet te veel haken en ogen heeft omdat onze doelgroep studenten zijn. Dit heb ik weer terug laten komen in mijn prototype. In de app zit een voedingsadvies gedeelte, een stappenteller en avatar.



**Redo van mijn Lifestyle Dairy en Ontwerpcriteria**



Afgelopen zondag heb ik nog het een en andere aangepast aan mijn Lifestyle Dairy. Bij de Validatie en het feedback wat heb ik gekregen op mijn Lifestyle Dairy miste ik dus een tijdlijn, chronologische volgorde en ook wat kleur. Nadat ik de tijdlijn op twee A3 vellen heb gemaakt (1 vel per dag, ik koos voor maandag en donderdag) had ik nog wat plaatjes opgezocht die ik zou gaan uitprinten en opplakken. Helaas bij het uitprinten thuis had mijn printer de plaatjes verpest. Ik wou het niet nog een keer uitprinten thuis omdat ik het inkt niet verder wou gaan verspillen daarom besloot ik dit op school toen en daarna verder af te maken en daarna nog wat kleur er bij toe te voegen.

Ook had ik zondag het ontwerpcriteria aangepast aan de hand van het feedback wat ik had gekregen. De docent had gezegd dat ik moest gaan kijken naar zijn PowerPoint presentatie waarin nog meer goede voorbeelden stonden van wat een ontwerpcriteria nou precies is. Deze heb ik goed doorgenomen en daarna toegevoegd aan mijn eigen ontwerpcriteria. Maandag ga ik nog een keer naar de validatie en dan kan ik zien of ik het uiteindelijk goed gedaan heb.



**Feedback Lifestyle Dairy versie 2 en paper prototype en tweede validatie ontwerpcriteria**



Maandag nadat ik mijn Lifestyle Dairy snel in elkaar had gezet ben ik naar de studio docent gegaan om feedback te vragen over mijn  twee versie van mijn Lifestyle Dairy. De docent zei dat ik hem dit keer beter en duidelijker gemaakt had alleen miste hij nog wat kleur. op dit moment had ik mijn kleurpotloden niet bij mij en ga ik later deze week dat nog eventjes toevoegen. Verder had ik aan dezelfde docent ook feedback gevraagd over mijn paper prototype. Hij zei dat mijn prototype er goed uitzag en dat hij ook duidelijk te begrijpen was. Nadat ik dit feedback had gehad heb ik mijzelf ingeschreven voor de validatie voor de paper prototype van dinsdag.

Later in de middag was ik naar het tweede validatie moment van het ontwerpcriteria gegaan. Daar zat dezelfde docent van het vorige validatie moment van het ontwerpcriteria. Hij was dit keer tevreden over het resultaat wat ik hem liet zien. Dit keer heeft hij mijn ontwerpcriteria goedgekeurd en kan ik het gebruiken voor mijn leerdossier.



**Validatie paper prototype**


Dinsdag in de ochtend ik vroeg opgestaan om mijn spiekbrief af te ronden voor het tentamen Design Theory. Deze is aanstaande donderdag. Later op de dag was ik naar de validatie gegaan van mijn paper prototype. Daar heb ik te horen gekregen dat mijn prototype niet helemaal in orde was. Ik had het volgende feedback gekregen;



	- **De prototype lijkt meer op een app en niet op een prototype. Het was niet valide genoeg om de functionaliteit vast te kunnen stellen. Verder was mijn prototype wel zodanig uitgewerkt dat het motiverend is voor een testpersoon. Als verdere feedback heb ik mij gekregen dat ik met degene die het testplan had gemaakt binnen mijn team, dat ik daar eerst mee had moeten overleggen voordat ik mijn paper prototype maakte.**



Dit feedback heb genoteerd en daarna gelijk in mijn leerdossier gezet omdat ik hier toch wat uit kan leren.


**Laatste feedback momenten voor mijn leerdossier**



Woensdag heb ik nog wat feedback gevraagd en daarbij een handtekeningen laten zetten zodat ik het kon gebruiken voor mijn leerdossier. Ik had feedback gevraagd op mijn gebruikersprofiel en ook op mijn leerdossier. Ik wou feedback op mijn leerdossier omdat ik er nog al erg onzeker over was. Ik wou weten of ik wel goed op weg was bij het schrijven van min STARRT's. Gelukkig bleek dat wel het geval te zijn nadat ik mijn gesprek hierover had gehad.



**Tentamen Design Theory en afrondingen leerdossier OP3**

Vandaag had ik in de ochtend het tentamen van Design Theory 3. Ik moet zeggen dat het ik het tentamen goed te doen vond. Toen ik thuis was gekomen heb ik mijn leerdossier afgemaakt en daarna ingeleverd.
