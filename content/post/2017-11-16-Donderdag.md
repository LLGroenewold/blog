﻿
---

title: Een thuiswerk dag

date: 2017-11-16

---


## Donderdag 16 November 2017



**Bespreken en werken aan de debrief**


Vandaag omdat er geen werk college was en er ook geen reden was om naar school te komen, hebben wij als groep besloten om thuis te werken aan onze opdrachten.  ik  heb gewerkt aan de debrief samen met Josephine. Ik had een bestand aangemaakt op Google Drive en besproken met Josephine via Whatsapp wie welk deel van de debrief ging schrijven. We hadden dit gedaan aan de hand van de vragen die we moesten beantwoorden in de debrief. Ik heb daarna mijn deel getypt op Google Documents dat daarna automatisch werd ge-update op Google Drive. Josephine heeft daarna haar deel daar aan toegevoegd.  Toen hebben we het doorgenomen en heeft de rest van het groepje er naar gekeken. We hebben ook besloten om aankomende maandag er feedback om te vragen aan de desbetreffende docent.

![](Debrief.png)