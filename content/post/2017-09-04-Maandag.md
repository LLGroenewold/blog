﻿---
title: Eerste Project Dag
date: 2017-09-04
---

## Maandag 4 september 2017

-	Als team hebben wij een keuze gemaakt wie onze teamcaptain zou zijn dit kwartaal. We hebben gekozen voor *Xander van Riet*. Daarna hebben wij de regels en de planning vastgesteld. Vervolgens hebben daarna een lijst van elkaars kwaliteiten gemaakt. 
-	Ook hebben we besloten elke **vrijdag om 12:00** een mini vergadering te houden om ze te kijken hoe alle er voor staat. Dit doen wij in de mediatheek bij WdKA.
-	We hebben ook deels de doelgroep vastgesteld. **“Eerstejaars kunststudenten”**
-	Daarna ben ik begonnen aan een woord-web met als kernwoord “kunststudenten” om zo tot ideeën te komen voor wat interview vragen voor eerstejaars kunststudenten. Met deze vragen willen wij zo meer ideeën en inspiratie op doen voor onze game.
-	Verder had ik als opdracht gekregen om te zoeken naar architectuur werken in Rotterdam.

![](Maandag_Wordenweb1.jpg)
![](Maandag_Wordenweb2.jpg)
![](Maandag_screenshot.jpg)