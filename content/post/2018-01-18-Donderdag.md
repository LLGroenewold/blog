﻿
---

title: Reflectie

date: 2018-01-18

---



## Donderdag 18 januari



**Reflectie op dit project en de samenwerking**



Deze periode vond ik erg goed gaan. Vooral in de samenwerking ging het heel veel beter in vergelijking met de vorige groep waar ik in zat. Bianca, Donovan en Josephine waren erg aardig en hielden goed contact. Ook in de samenwerking liep alles vlekkeloos. Soms kon niet iedereen aanwezig zijn vanwege een ziekte of een andere omstandigheden maar dan werd dat alsnog op tijd gezegd. In mijn vorige groepje was dat niet zo. Verder  heb ik over de samenwerking niks op of aan te merken. Het ging gewoon prima.

Verder heb ik in deze periode veel geleerd en veel beroepsproducten gemaakt in samenwerking met mijn groepje. Wat nieuw was waren de validaties voor de beroepsproducten. Ik weet dat het wel belangrijk is dat deze gedaan worden zodat je werk van feedback is voorzien en zo maar de wachttijden en het inschrijven vond ik alleen wat minder gaan. Ik hoop dat dit misschien een keer digitaal kan gebeuren zodat dat ook makkelijker is voor iedereen.

Verder heb vond ik de workshops die ik heb gevolgd erg leerzaam en ik heb er zeker wat aangehad. Scrum was handig voor het plannen en bijhouden van de beroepsproducten en letterontwerp is gewoon over het algemeen handig wat over te weten.

Kortom deze periode ging een heel stuk beter dan de vorige.
