---

Title: Speciale blog - Keuzevak Fablab Making

date: 2019-04-11

---

# De Bijzettafel lamp - gemaakt door Lindsay Groenewold


## Inhoud


## 1 Instructable Bijzettafellamp


1. **Tekening, afmetingen en overzicht van de materialen**
2.  **Lijst met benodigdheden Circuit**
3.  **Lijst met benodigdheden Materiaal**
4.  **Apparaten/gereedschappen die je nodig hebt**
5.  **Lijst met bestanden**
6.  **Proces van het maken van de lamp**
    -   **Lasersnijden**
    -   **3D printen**
    -   **Solderen**
    -   **In elkaar zetten**
    -   **Coderen**
7.  **Resultaat**
8.  **Voorkomende fouten**


## 2 Resultaten van tijdens de Fablab lessen


1.  **Prototype**
2.  **Leren omgaan met Tinkercad en 3D printen**
3.  **Laser snijden**
4.  **Acryl Lasersnijden**
5.  **Elektrisch Circuit maken in Tinkercad + code (versie 1)**
6.  **Resultaat versie 1 van de lamp**


==============================================================

# 1 Instructable Bijzettafellamp


**Welkom bij de instructable van de bijzettafel lamp! Je gaat een mooie bijzettafel maken die ook als lamp dient. Veel plezier bij het maken!** ![](https://confluence.hr.nl/s/nl_NL/7901/119ca25093da5dbe5f504cc7d71e386a1aab2736/6.12.2/_/images/icons/emoticons/biggrin.svg)


## 1 Tekening, afmetingen en overzicht van de materialen

**Hier is een tekening van de bijzettafel lamp en hoe hij er ongeveer uit gaat te komen zien.**
 **![](https://confluence.hr.nl/download/attachments/103317838/Tekening.jpeg?version=1&modificationDate=1552227771540&api=v2 "CMI Public > Lindsay Groenewold > Tekening.jpeg") ![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190216_211842.jpg?version=1&modificationDate=1551368867017&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190216_211842.jpg")**
 
**Dit zijn de afmetingen van de bijzettafellamp**


1.  **De "lampenkap" wordt 13 cm bij 23 cm deze moet uiteindelijk 4 keer worden uitgesneden. De lampenkap krijgt een bladpatroon**
2.  **De voet van de bijzettafellamp wordt een soort doos. De boven en onderkant van de voet worden 20 cm bij 20 cm, deze moet 2 keer worden uitgesneden. De randen van de voet worden 5 cm bij 20 cm en moet 4 keer worden uitgesneden.**
3.  **Het tafelblad wordt 25 cm bij 25 cm, deze moet 1 keer worden uitgesneden.**


## 2 Lijst met benodigdheden voor het circuit:


1. **Arduino Uno ([https://www.kiwi-electronics.nl/arduino-uno-rev3-atmega328?search=arduino&description=true](https://www.kiwi-electronics.nl/arduino-uno-rev3-atmega328?search=arduino&description=true) = € 23,95)** 
2.  **Breadboard ([https://www.kiwi-electronics.nl/breadboard-400pins?search=breadboard&description=true](https://www.kiwi-electronics.nl/breadboard-400pins?search=breadboard&description=true) = € 4,95 )**
3.  **Jumperwires M/M ([https://www.kiwi-electronics.nl/Premium-Jumperwires-40-stuks-op-strip-20cm-male-male?search=jumperwires&description=true](https://www.kiwi-electronics.nl/Premium-Jumperwires-40-stuks-op-strip-20cm-male-male?search=jumperwires&description=true) = € 4,95 per 40 stuks )**
4.  **1 x USB Kabel Type A/B (die zit altijd standaard bij de Arduino. Maar als dat niet het geval is dan kan je hem hier vinden: [https://www.kiwi-electronics.nl/USB-2-0-High-Speed-Kabel-USB-A-naar-USB-B-1-meter](https://www.kiwi-electronics.nl/USB-2-0-High-Speed-Kabel-USB-A-naar-USB-B-1-meter) € 2,75)**
5.  **1 x Weerstand 470 Ohm ([https://www.kiwi-electronics.nl/Weerstand-470-ohm-1-4-watt-5-procent-10-stuks?search=470%20ohm%20resistor&description=true](https://www.kiwi-electronics.nl/Weerstand-470-ohm-1-4-watt-5-procent-10-stuks?search=470%20ohm%20resistor&description=true) = € 1,00 per 10 stuks)**
6.  **1 x 1000uF 35V condensator ([https://www.kiwi-electronics.nl/1000uf-35v-condensator?search=condensator%201000&description=true](https://www.kiwi-electronics.nl/1000uf-35v-condensator?search=condensator%201000&description=true) = € 1,25 per stuk)**
7.  **1 x Neopixel Ring - 24 ([https://www.kiwi-electronics.nl/neopixel-ring-24x-ws2812-5050-rgb-led-met-drivers?search=neopixel&description=true](https://www.kiwi-electronics.nl/neopixel-ring-24x-ws2812-5050-rgb-led-met-drivers?search=neopixel&description=true) = € 19,95 )**    
8.  **1 x IR sensor - TSOP38238 ( [https://www.kiwi-electronics.nl/infrarood-sensor-ir-38khz-TSOP38238?search=ir%20sensor&description=true](https://www.kiwi-electronics.nl/infrarood-sensor-ir-38khz-TSOP38238?search=ir%20sensor&description=true) = € 1,75)**
9.  **1 x Afstandsbediening naar keuze (Ik had een afstandsbediening gebruikt van een LED kit van het merk DYMOND. Deze kit kun je halen bij de Action. [https://www.action.com/nl-nl/p/dymond-tv-ledstrip-/](https://www.action.com/nl-nl/p/dymond-tv-ledstrip-/) deze kit kost € 6,95)**
10. **Optioneel: 1 x 5 volt /1800 mAh powerbank ([https://www.action.com/nl-nl/p/powerbank-sleutelhanger-/](https://www.action.com/nl-nl/p/powerbank-sleutelhanger-/) = € 2,95)**


## 3 Lijst met Benodigdheden materiaal:


1.  **2 x Multiplex hout platen 400 x 700 x 6 mm (LET OP: Bij dit hout het proces van het lasersnijden lang kan gaan duren, het hout zal meerdere malen moeten woorden gesneden worden afhankelijk van de instellingen van de lasersnijder)**
2.  **Acrylglas van circa 3 mm (Het soort acrylglas wat ik heb gebruikt leek op reflector glas deze platen waren 34 cm x 34 cm. Van dit acrylglas had ik twee platen gebruikt.)**
3.  **Schuurpapier (het is belangrijk dat de alle uitgesneden stukken van de lamp worden geschuurd. Dit zorgt voor een mooier resultaat)**
    

  

## 4 Apparaten/gereedschappen die je nodig hebt:

1.  **Lasersnijder**
2.  **3D printer**
3.  **Soldeerbout**
4.  **Lijmpistool**
5.  **Secondelijm**
6.  **Schroevendraaier**
7.  **Stanleymes**
8.  **Schroevendraaier**
9.  **Kniptang**
10.  **Kleine schroefjes met een boutje er aan (inbus bol M3 x 20 mm)**
11.  **3 extra kleine boutjes**
12.  **Optioneel: Boormachine met een kleine boor**  
    

  

## 5 Lijst met bestanden:

1.  **Lasersnijbestanden:**
    
    -   **[Bijzettafellamp_Deel_1.ai](https://confluence.hr.nl/download/attachments/103317838/Bijzettafellamp_Deel_1.ai?version=1&modificationDate=1554893772747&api=v2)**  
    -   **[Bijzettafellamp_Deel_2.ai](https://confluence.hr.nl/download/attachments/103317838/Bijzettafellamp_Deel_2.ai?version=1&modificationDate=1554893775400&api=v2)** 
    -  **[Bijzettafellamp_Deel_3.ai](https://confluence.hr.nl/download/attachments/103317838/Bijzettafellamp_Deel_3.ai?version=1&modificationDate=1554893774327&api=v2)**

2.  **3D printbestand**

    -   **[Arduino houder.stl](https://confluence.hr.nl/download/attachments/103317838/Arduino%20houder.stl?version=2&modificationDate=1553945510363&api=v2)**  

3.  **Arduino/code bestanden:**
    -   **[Code_Van_Afstandsbediening_Krijgen.zip](https://confluence.hr.nl/download/attachments/103317838/Code_Van_Afstandsbediening_Krijgen.zip?version=1&modificationDate=1554894147590&api=v2)** *(Deze code is niet van mijn maar van Ken Shirriff. Ik heb deze gebruikt om de hex code van mijn afstandsbediening te krijgen. Het is van zeer belang dat je dit doet als een andere afstandsbediening gebruikt.)*  
    -   **[DYMOND_Receiver_BijzettafelLamp.zip](https://confluence.hr.nl/download/attachments/103317838/DYMOND_Receiver_BijzettafelLamp.zip?version=1&modificationDate=1554894157470&api=v2)**
    -   **[libraries.zip](https://confluence.hr.nl/download/attachments/103317838/libraries.zip?version=1&modificationDate=1554894405713&api=v2)** *(dit zijn de libraries die je nodigt hebt voor de codes. Zonder de libraries kun je de code niet gebruiken. Kopieer en plak de inhoud van deze map in je libraries map van je Arduino map zodat je ze kunt gebruiken)*

## 6 Proces van het maken van de lamp

  

## 1 Lasersnijden

-   **Acrylglas - bestand: [Bijzettafellamp_Deel_3.ai](https://confluence.hr.nl/download/attachments/103317838/Bijzettafellamp_Deel_3.ai?version=1&modificationDate=1554893774327&api=v2)**

Het eerste wat je gaat doen is lasersnijden. Zorg dat je al je materiaal en begin dan eerst met het acrylglas. Wat ik heb gedaan met mijn acrylglas is dat de geribbelde kant op de snijplaat ligt van de lasersnijder. Stel de lasersnijder in op de juiste instellingen voor acrylglas en begin dan met snijden. Als je het zelfde formaat als mij hebt gebruikt, doe het proces dan nog een keer zodat je uiteindelijk 4 uitgesneden stukken hebt. (zie de foto's hier onder).

 **![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190314_102807.jpg?version=1&modificationDate=1553945109230&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190314_102807.jpg")![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190314_102744.jpg?version=1&modificationDate=1553945124377&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190314_102744.jpg")![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190314_103313.jpg?version=1&modificationDate=1553945112463&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190314_103313.jpg")**
-   **Multiplex hout platen - Bestanden: [Bijzettafellamp_Deel_1.ai](https://confluence.hr.nl/download/attachments/103317838/Bijzettafellamp_Deel_1.ai?version=1&modificationDate=1554893772747&api=v2) en [Bijzettafellamp_Deel_2.ai](https://confluence.hr.nl/download/attachments/103317838/Bijzettafellamp_Deel_2.ai?version=1&modificationDate=1554893775400&api=v2)**

Ga vervolgens verder met het hout. Stel de lasersnijder voor het 6 mm dik hout. Begin dan met snijden. Het proces kan lang gaan duren vanwege het blad patroon (zie de foto's hier onder). Het snijden van Deel 1 duurde de eerste keer bij mij een anderhalf uur. En ik moest daarna de lasersnijder het proces laten herhalen omdat de laser er de eerste keer niet doorheen kwam. Hetzelfde probleem had ik met Deel 2, het proces duurde niet zo lang.  

> **TIP**: *Als de lasersnijder na de eerste keer er niet gelijk door komt kun je de snijplaat van de lasersnijder een kleine stukje om hoog doen. Dit zorgt er voor dat de laser iets dieper in het hout snijd*.


![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190314_114744.jpg?version=2&modificationDate=1554980277830&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190314_114744.jpg") ![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190314_140850.jpg?version=2&modificationDate=1554980304980&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190314_140850.jpg")

## 2 3D printen

-   **Op de Ultimaker 2+ - Bestand: [Arduino houder.stl](https://confluence.hr.nl/download/attachments/103317838/Arduino%20houder.stl?version=2&modificationDate=1553945510363&api=v2)**

Terwijl de lasersnijder bezig is met het uitsnijden van het hout kun je de 3D printer aanzetten. Stel de 3D printer in op de juiste instellingen en print dit uit. Het duurde bij mij ongeveer 43 minuten om dit uit te printen maar dat hangt natuurlijk af van welke 3D printer je gebruikt. Ik had de Ultimaker 2+ gebruikt voor dit.  
      
 Als je printje klaar is moeten de gaten die er in zitten een beetje vergroot worden. Deze gaatjes zijn voor de schroefjes waarmee je de Arduino gaat bevestigen aan de lamp. Ik de gaatjes vergroot met een schroevendraaier, ik bleef constant draaien en keek vervolgens of de schroefjes er doorheen paste. Ook moet de "basis" die er om heen zit afgebroken of gesneden worden. Dit kun je doen met een stanleymes of een scherpe kniptang.

**![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190314_105025.jpg?version=1&modificationDate=1553945724400&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190314_105025.jpg")![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190314_121208.jpg?version=1&modificationDate=1553945727730&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190314_121208.jpg")![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190314_131522.jpg?version=1&modificationDate=1553945735193&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190314_131522.jpg")**

## 3 Solderen

Wanneer je alles hebt uitgesneden en hebt uitgeprint kun je gaan beginnen met solderen. Voor dit heb je drie lange jumperwires nodig van 20 cm en je Neopixel Ring. Deze lengte is nodig omdat je de draadjes door een gaatje moet verbinden op het breadboard en aan de Arduino. Voor de makkelijkheid probeer 3 verschillende kleuren te gebruiken. Ik had een rode jumperwire gebruikt voor de stroom/5v, een zwarte jumperwire voor GND en een witte jumperwire voor signaal.

![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190319_180039.jpg?version=1&modificationDate=1553954502540&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190319_180039.jpg")

## 4 In elkaar zetten

Voordat je de lamp in elkaar gaat zetten begin met het schuren van je houten onderdelen. Schuur ze tot dat je er tevreden mee bent. Ik had geschuurd tot dat ik alle "brand" plekken van het hout af waren en tot het hout "glad" was. Nadat je alles hebt geschuurd is het tijd om onderdelen aan elkaar te lijmen. Zet je lijmpistool aan en laat hem opwarmen tot dat het lijm goed vloeibaar is. Vervolgens zet je je gesneden acrylglas op de stukken hout waar het bladpatroon op zit. Je kunt hier als je wilt klemmen voor gebruiken zodat het op z'n plaats blijft maar ik heb dat niet gedaan. Wanneer je het acrylglas goed gepositioneerd hebt kun er om heen gaan met de lijmpistool. probeer niet te veel lijm naar buiten te laten kom want anders kunnen stukken hout niet meer op elkaar aansluiten bij het in elkaar zetten. Mocht dat toch gebeuren dan kun je nog altijd met een platte schroevendraaier en een stanleymes het proberen weg te schrappen.

![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190314_160129.jpg?version=1&modificationDate=1553954797093&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190314_160129.jpg")![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190314_160138.jpg?version=1&modificationDate=1553954808253&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190314_160138.jpg")![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190410_161004.jpg?version=1&modificationDate=1554910709297&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190410_161004.jpg")

De volgende stap die moet doen is het bevestigen van de 3D printje, je Arduino, je breadboard en pootjes. Het 3D printje moet in principe op de zelfde manier bevestigen als het acrylglas. Alleen als je goed gekeken hebt bij het uitsnijden van het hout, had misschien al gezien dat er een graveert patroon is waar je je dit printje moet neerzetten. Positioneer het 3D printje op het gegraveerde stuk en ga er omheen met de lijmpistool. Probeer ook hier niet te veel lijm te gebruiken. Wanneer je dat gedaan hebt bevestig het breadboard er naast. Dit is makkelijk te doen door het dubbelzijdig tape van het breadboard te gebruiken. Druk het breadboard nog even aan het is klaar.

### ![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190410_160706.jpg?version=1&modificationDate=1554910738957&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190410_160706.jpg")


Nu is het tijd om de pootjes te bevestigen. Dit kun je doen met seconde lijm en de lijmpistool. Smeer de pootjes voorzichtig in met secondelijm en stop ze vervolgens in de gaatjes waar ze thuis horen. Laat ze even een minuutje of twee drogen. Ga daarna er vervolgens over heen met de lijmpistool. Wanneer het lijm nog zacht is kun je het proberen voorzichtig uit te smeren met je vinger zodat de lijm goed verdeeld is over de pootjes en het hout. Maar wees wel erg voorzichtig dat je je zelf niet verbrand. Je kunt het ook doen met een stukje karton of iets dergelijks.


![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190410_160713.jpg?version=1&modificationDate=1554910775353&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190410_160713.jpg")

Nu ga je het elektrisch circuit bevestigen en zoetjes aan de lamp in elkaar zetten. Als eerste ga je de Arduino vast maken. Pak je 3 schroefjes met 3 boutjes, steek de schroefjes van onder het hout door de 3 gaatjes heen en schroef ze daarna vast met de boutjes zodat ze niet lost komen. 


> **TIP**: *Mocht je de schroefjes er niet door heen krijgen dan moet je proberen de gaatjes te vergroten met een boormachine met een kleine boor. Bij was dit wel het geval en moest ik de gaatjes vergroten*


Wanneer je de schroefjes hebt vastgezet zet je de Adruino er open maak je hem vast met de andere 3. Dit zorgt er voor dat hij niet verschuift of los komt.


**![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190410_160732.jpg?version=1&modificationDate=1554910758647&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190410_160732.jpg")![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190410_161448.jpg?version=1&modificationDate=1554910830073&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190410_161448.jpg")**


Nu is het tijd voor het elektrisch circuit. Hier is de layout van het circuit:


### **![](https://confluence.hr.nl/download/attachments/103317838/Neopixel%20with%20IR%20remote%20control.png?version=3&modificationDate=1554898969780&api=v2 "CMI Public > Lindsay Groenewold > Neopixel with IR remote control.png")**


Probeer dit plaatje goed te volgen om een goed beeld te krijgen van hoe de het elektrisch circuit in elkaar zit. Probeer de zelfde kleuren jumperwires te gebruiken om het je zelf makkelijker te maken. Het is het beste om wat langere jumper wires te gebruiken voor het gehele circuit. Anders ga je een probleem krijgen met het dicht maken van de bodem box. Langere jumperwires kunnen een beetje buigen.


1.  Sluit de **5V en GND** van de Arduino naar het breadboard
2.  Sluit **pin 2** aan op het breadboard (dit is de signaal pin voor de IR sensor)
3.  Sluit **pin 6~** aan op het breadboard (dit is de signaal pin voor de Neopixel Ring)
4.  Sluit de **gepolariseerde condensator (1000uF 35V condensator)** aan op het breadboard **(**de condensator zorgt er voor dat de Neopixel niet doorbrand) **(LET OP: SLUIT HEM GOED AAN! OP HET PLAATJE STAAT DE STREEP NAAR LINKS. DUS DOE DAT OOK)**
5.  **Vanaf de condensator:** zet een jumperwire van de kant met de streep naar en - (min) kant van het breadboard en zet een jumperwire van de kant zonder streep naar +(plus) kant van het breadboard
6.  Zet de **weerstand van 470 ohm** vanaf de signaal kabel uit pin 6~ neer
7.  Sluit **de IR sensor** aan
8.  **Vanaf de IR sensor:** sluit de jumperwires van links naar rechts: links: signaal (pin2) midden: GND (naar de -(min) kant van het breadboard) rechts: 5V (naar de + (plus) kant van het breadboard)
9.  Voordat je je Neopixel aansluit, zet de bodem van de lamp in elkaar. Kijk goed naar de stukken voordat je hem in elkaar zet. Er is een stuk met een gat, dit gat is voor de USB kabel. Sluit deze ook gelijk aan.
10.  Sluit nu je Neopixel Ring aan. Doe de gesoldeerde jumperwires door het gat van de stuk hout met de graveerde cirkel en sluit de jumperwires aan zoals boven aangegeven is op de afbeelding.
11.  Doe nu het stuk hout boven op de bodem zodat je de Arduino niet meer ziet.

**![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190410_160838.jpg?version=1&modificationDate=1554911008437&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190410_160838.jpg")![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190410_162316_1.jpg?version=1&modificationDate=1554911052477&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190410_162316_1.jpg")![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190410_163816.jpg?version=1&modificationDate=1554911038217&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190410_163816.jpg")![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190410_160757.jpg?version=2&modificationDate=1554910984493&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190410_160757.jpg")![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190410_162231.jpg?version=1&modificationDate=1554911063267&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190410_162231.jpg")![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190410_164034.jpg?version=1&modificationDate=1554910939047&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190410_164034.jpg")![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190330_150954.jpg?version=1&modificationDate=1553957230360&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190330_150954.jpg")**

## **6 Coderen**


Voor dat je gaat beginnen met coderen ga je eerste de libraries toevoegen. Download het zip bestandje **( [libraries.zip](https://confluence.hr.nl/download/attachments/103317838/libraries.zip?version=1&modificationDate=1554894405713&api=v2) )** en pak het uit. Je ziet het mapje "libraries" open het mapje en kopieer/knip alle mapjes en plak ze in je eigen libraries mapjes in je Arduino map op je computer. Als je dat gedaan hebt download je de twee andere zip bestanden **([Code_Van_Afstandsbediening_Krijgen.zip](https://confluence.hr.nl/download/attachments/103317838/Code_Van_Afstandsbediening_Krijgen.zip?version=1&modificationDate=1554894147590&api=v2) en [DYMOND_Receiver_BijzettafelLamp.zip](https://confluence.hr.nl/download/attachments/103317838/DYMOND_Receiver_BijzettafelLamp.zip?version=1&modificationDate=1554894157470&api=v2))** de twee mapjes die je daar uit krijgt zet je ook in je Arduino mapje op je laptop.


**![](https://confluence.hr.nl/download/attachments/103317838/Screenshot%201.png?version=1&modificationDate=1554911451073&api=v2 "CMI Public > Lindsay Groenewold > Screenshot 1.png")![](https://confluence.hr.nl/download/attachments/103317838/screenshot%202.png?version=1&modificationDate=1554911451213&api=v2 "CMI Public > Lindsay Groenewold > screenshot 2.png")**


Sluit de USB kabel aan je computer aan en open het Arduino IDE programma. Open eerst het bestand **"Code_Van_Afstandsbediening_Krijgen"**. Zet deze op je Arduino en open ook de serial monitor. Wanneer de code op je Arduino staat pak je je afstandsbediening. Ook is het verstandig als je je noties op je computer open zet. Nu ga je knoppen indrukken op je afstandsbediening. Bij elke knop die je indrukt op je afstandsbediening zie je een code verschijnen op de serial monitor. De codes waar je voor wilt gaan zien er zo uit **Received XXX: XXXXXX.** In mijn geval ging het om een NEC afstandsbediening en zag mijn code er zo uit: **Received NEC: 61D640BF.** Hier is een screenshot van het proces en een foto van de afstandsbediening die ik heb gebruikt:


![](https://confluence.hr.nl/download/attachments/103317838/code%20van%20de%20afstandsbediening%20krijgen.png?version=1&modificationDate=1554911726350&api=v2 "CMI Public > Lindsay Groenewold > code van de afstandsbediening krijgen.png")![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190410_162850.jpg?version=1&modificationDate=1554911747310&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190410_162850.jpg")


Hier is een tabel met de Hex en RGB codes van de knoppen.


![](https://confluence.hr.nl/download/attachments/103317838/Tabel%20Dymond%20afstandsbediening.png?version=1&modificationDate=1554990030448&api=v2 "CMI Public > Lindsay Groenewold > Tabel Dymond afstandsbediening.png")


Als je een andere afstandsbediening hebt gebruikt dan zal de code iets anders aangeven in plaats van **NEC**. Ik haddeze code ook gebruikt met een **SONY** afstandsbediening om te kijken hoe die code eruit zag. In het geval van een **SONY** afstandsbediening zal er in de code van de serial monitor ook **SONY** staan. Onthou dit want je hebt het nodig voor de volgende code.


Wanneer je de code hebt van je afstandsbediening open je het bestand van de code van "**DYMOND_Receiver_BijzettafelLamp".** Deze code is gebonden aan de afstandsbediening die ik heb gebruikt voor dit project. Maar de code is heel makkelijk aan te passen aan een andere afstandsbediening. Als je de code hebt geopend en je wilt de code aanpassen naar jou afstandsbediening naar keuze scroll dan een stukje naar bedienden tot dat je naar bij de void loop aangekomen ben. De eerste paar regels tekst die je dan ziet zijn de belangrijkste stukjes tekst van de code die je nodig hebt om het aan te passen naar jou afstandsbediening:

 

> void loop() { 
> if (myReceiver.getResults()) { 
> myDecoder.decode();  
>if (myDecoder.protocolNum ==NEC) {
> switch(myDecoder.value) {


Je ziet **NEC** staan. Vervang dit naar bijvoorbeeld **SONY** als je een Sony afstandbediening hebt.



![](https://confluence.hr.nl/download/attachments/103317838/screenshot3.png?version=1&modificationDate=1554912380580&api=v2 "CMI Public > Lindsay Groenewold > screenshot3.png")


Als je de HEX codes wilt aanpassen van de code naar de knoppen van je afstandsbediening dan moet je een stukje verder naar beneden scrollen dan zie je steeds dit soort codes:


> case 0x61D648B7: //OFF Button for
> (int i=0; i<24; i++) // This part of the code adds how many LEDs should be turned on (0, R, G, B) { 
>strip.setPixelColor(i, 0, 0, 0); //Turns the light off**  }  
> break;
De regel met het "`case 0x61D648B7: //OFF Button:`" is het stukje waar je je code moet vervangen. Vervang `**61D648B7**` met jou hex code. In dit geval gaat het om een uit knop. Het is ook verstandig als je er achter zet om wel knop het gaat. doe dit met // dan zorg je er voor dat het als een notitie in je code staat. Als je de kleur wilt aan passen dan moet je in de regel waar: "`strip.setPixelColor(i, 0, 0, 0); //Turns the light off` "staat aan de slag gaan. Vervang de drie nullen met een RGB code naar keuze, bijvoorbeeld zo: `"strip.setPixelColor(i, 0, 255, 255); //Cyan`" .Wil je meer knoppen en kleuren toevoegen?? Kopieer en plak de code dan gewoon onder elkaar en herhaal het proces van de hex code en de RGB code vervangen met een andere kleur. Wil je minder kleuren? Dan verwijder je dit stukjes van de code.


![](https://confluence.hr.nl/download/attachments/103317838/screenshot4.png?version=1&modificationDate=1554912912393&api=v2 "CMI Public > Lindsay Groenewold > screenshot4.png")


Wanneer je klaar bent met het aanpassen van de code kun je deze uploaden naar je Arduino. Daarna kun je de lamp helemaal in elkaar zetten.

  

![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190410_162516.jpg?version=1&modificationDate=1554914359123&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190410_162516.jpg")![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190410_163212.jpg?version=1&modificationDate=1554914274957&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190410_163212.jpg")![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190410_163309.jpg?version=1&modificationDate=1554914309417&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190410_163309.jpg")![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190410_163335.jpg?version=1&modificationDate=1554914326600&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190410_163335.jpg")


## 7 Resultaat


Als je je lamp helemaal in elkaar hebt gezet is het tijd om hem aan te zetten. Dit kun je het beste testen wanneer het donker is voor een mooi resultaat.


<iframe width="700" height="394" src="https://www.youtube.com/embed/LTeHW4ULDpk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


**![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190410_164243.jpg?version=1&modificationDate=1554914577720&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190410_164243.jpg")**

## **8 Voorkomende fouten**


He
t kan voorkomen dat de afstandsbediening het niet doet. Ik weet niet of dit ligt aan de code, de Arduino of de afstandsbediening. Ik heb geprobeerd te achterhalen waar de fout light maar dit heb ik niet gevonden. Als het voorkomt dat de afstandsbediening het niet helemaal doet moet je lamp een paar keer resetten door de USB kabel opnieuw in je computer/powerbank te doen of de code opnieuw naar je Arduino sturen. Dat is de enige oplossing die ik heb gevonden.

### And that's it! De bijzettafel lamp is af! Ik hoop dat je veel plezier hebt gehad bij het maken van de lamp! **![(grote grijns)](https://confluence.hr.nl/s/nl_NL/7901/119ca25093da5dbe5f504cc7d71e386a1aab2736/_/images/icons/emoticons/biggrin.svg)**


===================================================================================


# 2 Resultaten van tijdens de Fablab lessen


# **1 Prototype**


![](https://confluence.hr.nl/download/attachments/103317838/IMG-20190216-WA0004.jpeg?version=2&modificationDate=1551371730233&api=v2 "CMI Public > Lindsay Groenewold > IMG-20190216-WA0004.jpeg")![](https://confluence.hr.nl/download/attachments/103317838/IMG-20190216-WA0002.jpeg?version=1&modificationDate=1551371736067&api=v2 "CMI Public > Lindsay Groenewold > IMG-20190216-WA0002.jpeg")![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190219_174314.jpg?version=1&modificationDate=1551371741720&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190219_174314.jpg")![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190219_174325.jpg?version=1&modificationDate=1551371751000&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190219_174325.jpg")


# 2 Leren omgaan met Tinkercad en 3D printen


## De bijzettafel lamp in 3D maken


### Bestanden:

-   **[Bijzettafellamp in 3D.zip](https://confluence.hr.nl/download/attachments/103317838/Bijzettafellamp%20in%203D.zip?version=1&modificationDate=1552230297867&api=v2)**
-   **[Lampenkap blah.svg](https://confluence.hr.nl/download/attachments/103317838/Lampenkap%20blah.svg?version=1&modificationDate=1552230490237&api=v2)**


Tijdens de les over Tinkercad had ik mijn bijzettafellamp in 3D nagemaakt. Er werd verteld dat je 2D tekeningen vanuit Adobe Illustrator kan importeren en deze gebruiken in Tinkercad. Ik had dat gedaan met mijn bladpatroon. Ik had deze gemaakt in Adobe Illustrator en vervolgens heb ik deze opgeslagen als een .SVG bestand. Daarna heb ik hem op de juiste grote geschaald.


![](https://confluence.hr.nl/download/attachments/103317838/Screenshot%20van%20tinkercad%201.png?version=1&modificationDate=1552230354220&api=v2 "CMI Public > Lindsay Groenewold > Screenshot van tinkercad 1.png")![](https://confluence.hr.nl/download/attachments/103317838/Screenshot%20van%20tinkercad%202.png?version=1&modificationDate=1552230354670&api=v2 "CMI Public > Lindsay Groenewold > Screenshot van tinkercad 2.png")


![](https://confluence.hr.nl/download/attachments/103317838/Screenshot%20van%20tinkercad%203.png?version=1&modificationDate=1552230354977&api=v2 "CMI Public > Lindsay Groenewold > Screenshot van tinkercad 3.png")![](https://confluence.hr.nl/download/attachments/103317838/Screenshot%20van%20tinkercad%204.png?version=1&modificationDate=1552230357050&api=v2 "CMI Public > Lindsay Groenewold > Screenshot van tinkercad 4.png")


## 3D printen


### Bestanden:


-   **[Cat_Paw.stl](https://confluence.hr.nl/download/attachments/103317838/Cat_Paw.stl?version=1&modificationDate=1552230603453&api=v2)**
-   **[Catpaw.svg](https://confluence.hr.nl/download/attachments/103317838/Catpaw.svg?version=1&modificationDate=1552230626887&api=v2)**


Voor de les over 3D printen had ik in Adobe Illustrator eerst iets gemaakt en vervolgens weer in Tinkercad geïmporteerd om het daarna gereed te maken voor de 3D print les.

  
![](https://confluence.hr.nl/download/attachments/103317838/Catpaw%20screenshot.png?version=1&modificationDate=1552230612467&api=v2 "CMI Public > Lindsay Groenewold > Catpaw screenshot.png")![](https://confluence.hr.nl/download/attachments/103317838/Screenshot%20cat%20paw%20tinkercad%201.png?version=1&modificationDate=1552230612590&api=v2 "CMI Public > Lindsay Groenewold > Screenshot cat paw tinkercad 1.png")![](https://confluence.hr.nl/download/attachments/103317838/Screenshot%20cat%20paw%20tinkercad%202.png?version=1&modificationDate=1552230612883&api=v2 "CMI Public > Lindsay Groenewold > Screenshot cat paw tinkercad 2.png")

  

## Resultaat:


![](https://confluence.hr.nl/download/attachments/103317838/cat%20paw%20final.jpeg?version=1&modificationDate=1552231195193&api=v2 "CMI Public > Lindsay Groenewold > cat paw final.jpeg")


# 3 Leren Lasersnijden

## Bestanden:


-   **[Lampen kap ding oefenen.ai](https://confluence.hr.nl/download/attachments/103317838/Lampen%20kap%20ding%20oefenen.ai?version=1&modificationDate=1552231338793&api=v2)**


Bij het lasersnijden moesten we werken met Adobe Illustrator. Ik had mijn bladpatroon in Illustrator gemaakt en ik had deze vervolgens gereed gemaakt voor de lasersnijder door middel van de settings die ik moet instellen voor de lijntjes van mijn patroon. Ik had mijn lijndikte op 0,01 mm gezet en de lijnkleur op rood. Vervolgens had ik mijn bestand opgeslagen en naar de lasersnijder gebracht.


**![](https://confluence.hr.nl/download/attachments/103317838/lasersnijden%20oefenen%20lampenkap.png?version=1&modificationDate=1552231576027&api=v2 "CMI Public > Lindsay Groenewold > lasersnijden oefenen lampenkap.png")![](https://confluence.hr.nl/download/attachments/103317838/Lasersnijden%201.jpeg?version=1&modificationDate=1552231658577&api=v2 "CMI Public > Lindsay Groenewold > Lasersnijden 1.jpeg")![](https://confluence.hr.nl/download/attachments/103317838/Lasersnijden%202.jpeg?version=1&modificationDate=1552231658750&api=v2 "CMI Public > Lindsay Groenewold > Lasersnijden 2.jpeg")**

## Resultaat:

**![](https://confluence.hr.nl/download/attachments/103317838/lasercutting%20final%201.jpeg?version=1&modificationDate=1552231701663&api=v2 "CMI Public > Lindsay Groenewold > lasercutting final 1.jpeg")![](https://confluence.hr.nl/download/attachments/103317838/lasercutting%20final%202.jpeg?version=1&modificationDate=1552231701820&api=v2 "CMI Public > Lindsay Groenewold > lasercutting final 2.jpeg")**


Het resultaat van het lasersnijder heeft me tot de conclusie gebracht dat ik voor mijn eindproduct dat ik hout van een dikte met 6 mm wil gaan gebruiken omdat het lekker stevig. Ik ben zeer blij met eind resultaat van mijn eerste keer lasersnijden.


# 4 Leren Acryl Lasersnijden


## Bestanden:

-   **[Dark runestone.ai](https://confluence.hr.nl/download/attachments/103317838/Dark%20runestone.ai?version=1&modificationDate=1552428470007&api=v2)**


  
Tijdens de tweede keer dat we mochten lasersnijden heb ik er voor gekozen om acryl te gaan lasersnijden. Dit keer heb ik ook iets laten graveren. Ik had een maan laten graveren op het acryl.


![](https://confluence.hr.nl/download/attachments/103317838/Proces%20van%20lasersnijden%20acryl.jpeg?version=1&modificationDate=1552426463497&api=v2 "CMI Public > Lindsay Groenewold > Proces van lasersnijden acryl.jpeg")**![](https://confluence.hr.nl/download/attachments/103317838/Screenshot_Acryl_Illustrator_Bestand.png?version=1&modificationDate=1552428533520&api=v2 "CMI Public > Lindsay Groenewold > Screenshot_Acryl_Illustrator_Bestand.png")**


## Resultaat:


**![](https://confluence.hr.nl/download/attachments/103317838/Acryl%20Lasersnijden%20resultaat.jpeg?version=1&modificationDate=1552426463287&api=v2 "CMI Public > Lindsay Groenewold > Acryl Lasersnijden resultaat.jpeg")**


Dit is het resultaat van het laser gesneden acryl.


# **5 Elektrisch Circuit maken in Tinkercad**


### Bestanden:


-   **[neopixel_fire01.zip](https://confluence.hr.nl/download/attachments/103317838/neopixel_fire01.zip?version=1&modificationDate=1554890765877&api=v2)**


In Tinkercad kan je dus ook een bouwplaat maken van een elektrisch circuit en deze dus ook testen. Tijdens de les hebben dit dus gedaan. De docent had mijn aangeraden om te kijken naar iets met LED verlichting. Ik heb ben dus gaan Googlen naar project met LED verlichting die je kunt maken met de Arduino en het blijkt dus dat er iets is dat heet Neopixel. Neopixel komt in allerlei soorten en maten onder andere in de vorm van ring die bestaat uit 12, 16 en 24 RGB LED lampjes die allemaal individueel kunt besturen met de Arduino. De Neopixel ring is niet duur en is ook makkelijk in gebruik. Ik ben verder gaan Googlen naar projecten die de Neopixel gebruiken en ik ben er bij een gekomen die vuur simuleert. Ik wil deze heel graag gaan toepassen in mijn project en ga hem daarom na maken in Tinkercad. Tinkercad kan helaas gaan code voor simuleren van de Neopixel, ik heb daarom de code van project dat ik heb overgenomen in de Arduino IDE gedaan en vervolgens, als tekst, in Tinkercad gezet en daarna gekeken of hij het deed, en het werkte inderdaad echt (zie de plaatjes hieronder).

  

**![](https://confluence.hr.nl/download/attachments/103317838/Elektrisch%20Circuit%20Bijzettafellamp.png?version=1&modificationDate=1552426768330&api=v2 "CMI Public > Lindsay Groenewold > Elektrisch Circuit Bijzettafellamp.png")![](https://confluence.hr.nl/download/attachments/103317838/Het%20Circuit%20staat%20aan.png?version=1&modificationDate=1552426768783&api=v2 "CMI Public > Lindsay Groenewold > Het Circuit staat aan.png")![](https://confluence.hr.nl/download/attachments/103317838/Close-up.png?version=1&modificationDate=1552426768040&api=v2 "CMI Public > Lindsay Groenewold > Close-up.png")**


**Bron:** Ulbricht, R. (2017, 18 september). Arduino Slovakia - Simulation of fire. Geraadpleegd op 13 maart 2019, van [http://www.arduinoslovakia.eu/blog/2015/9/simulacia-ohna?lang=en](http://www.arduinoslovakia.eu/blog/2015/9/simulacia-ohna?lang=en)


(*dit is de website waar ik de originele bouwplaat en code van de vuur simulatie voor de Neopixel vandaan heb gehaald, is wel zo eerlijk dat ik dit vermeld ook al is het free-to-use* ![(grote grijns)](https://confluence.hr.nl/s/nl_NL/7901/119ca25093da5dbe5f504cc7d71e386a1aab2736/_/images/icons/emoticons/biggrin.svg))


# 6 Resultaat versie 1 van de lamp



![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190330_151015.jpg?version=1&modificationDate=1553957234110&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190330_151015.jpg")![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190330_154251.jpg?version=1&modificationDate=1553957238490&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190330_154251.jpg")![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190330_154306.jpg?version=1&modificationDate=1553957244287&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190330_154306.jpg")![](https://confluence.hr.nl/download/attachments/103317838/IMG_20190330_154318.jpg?version=1&modificationDate=1553957223877&api=v2 "CMI Public > Lindsay Groenewold > IMG_20190330_154318.jpg")


Dit was de eerste versie van mijn bijzettafel lamp. Het verschil tussen deze versie en de nieuwe versie is dat de code anders is en de bodem box had nog geen gegraveerde tekening op de zijkanten. Ook is deze niet te besturen met een afstandsbediening.