---
Title: Reflectie Sprint 3
date: 2019-01-20
---

## Reflectie

In vergelijking  met vorig kwartaal is dit kwartaal beter verlopen. Ik vind zelf dat ik beter mijn best heb gedaan als het er op neer komt met samenwerking. Ik heb actiever meegedaan in activiteiten die we hebben gedaan als team en ook heb ik meer initiatieven genomen. Ik ben zelf trots op hoe ik heb gepresteerd.
