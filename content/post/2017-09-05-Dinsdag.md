﻿
---
title: Tweede Project Dag
date: 2017-09-05
---

## Dinsdag 5 september 2017

-	In de ochtend hebben we een hoorcollege gehad over **“Design Theory”** we hebben veel aantekenen moeten maken.

-	We hebben de definitieve doelgroep vastgesteld, **“eerstejaars fotografie studenten”**. We hebben dit gedaan omdat er meer over te vinden valt en we hebben er een leuker game idee bij.


-	In onze vrije tijd zijn we als groep naar de mediatheek gegaan in de WdKA.

-	We zijn verder gegaan met het brainstormen over het verzinnen van vragen voor ons interview.

-	Ik heb de eerste versie van mijn mood board gemaakt in Photoshop. Ik dit gedaan op een A3 formaat door middel van plaatjes en foto’s op te zoeken van onze doelgroep.


![](Moodboard_Versie1.png)

