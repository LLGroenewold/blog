---
title: Negende Project Dag
date: 2017-09-14
---

## Donderdag 14 september 2017

-	Toen ik op school kwam was 3/5 van het team niet aanwezig. In ochtend had Chanouk al gezegd dat ze ziek was. En Samantha had gisteren al vermeld dat ze zich niet helemaal lekker voelde en dat ze thuis bleef.  Alleen ik en Jascha waren aanwezig. We hebben geprobeerd contact te zoeken met onze teamcaptain Xander, maar hij liet pas VEEL later horen dat hij thuis bleef omdat hij zich niet lekker voelde. Dit was uiteraard erg vervelend voor ons aangezien we morgen de deadline hebben. Ik en Jascha hebben heel hard gewerkt om het prototype af te ronden en de rest van de overige opdrachten te maken.  Zoals de spel analyse. Het enige wat nu nog af moet is de eind pdf en de presentatie. Verder heb ik de rest van mijn individuele opdrachten afgerond en in een pdf gezet. Deze heb ik het ook ingeleverd.


![](Screenshot1.jpg)

![](Screenshot2.jpg)

![](Screenshot3.jpg) 