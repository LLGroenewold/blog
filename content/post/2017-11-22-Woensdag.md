﻿
---
title: Scrum
date: 2017-11-22
---
## Woensdag 22 November 2017

**Vragen voor de enquête bespreken**
Eerst in de ochtend hebben de vragen van de enquête besproken deze hebben daarna ook online gezet en verstuurd via Facebook, WhatsApp enz. Vervolgens zijn daarna verder gegaan met een concurrentie analyse en de user journey.

**Scrum workshop**
Later op de dag hebben ik en Bianca de workshop scrum gevolgd. We hebben hier veel aan gehad en hebben daarna zelf ook nog een scrum board gemaakt. Ik heb tijdens de workshop ook nog wat aantekeningen gemaakt en deze op de drive gezet zodat de rest van ons team deze ook nog kon lezen en zo begrijpen wat scrum allemaal inhoud.

![](enquête screenshot.png)
![](Facebook screenshot.png)
![](Scrum workshop aantekeningen.png)
![](scrumboard.jpg)

