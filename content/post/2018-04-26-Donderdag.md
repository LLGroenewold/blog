---
title: Interviewen en observeren
date: 2018-04-26
---
 
## De interviews en observatie

Maandag zijn we eerst bij elkaar gekomen op school om te luisteren naar de ochtend presentatie. Daarna zijn we onze interview vragen en observatie formulieren gaan uitprinten. Toen zijn we met de metro naar Maashaven gegaan om daar te gaan observeren. Toen we klaar waren met de observatie zijn we in groepen van twee door Tarwewijk gaan lopen om zo mensen te gaan interviewen.

Ik was samen met Nikola.  En Jenna en Leila waren samen. Ik ging eerst met Nikola naar het Millinxpark om daar interviews te houden maar op het tijdstip dat wij daar waren was nog niemand daar. Toen besloten we richten de Aldi te gaan om daar onze interviews te houden maar onderweg kwamen nog wat mensen van onze doelgroep tegen en deze hebben we kunnen interviewen. We hadden in totaal vijf mensen kunnen interviewen.  Jenna en Leila hadden niet echt iemand kunnen vinden om te interviewen. Dit kwam mede doordat het tijdstip dat wij waren gaan interviewen nog al vrij vroeg was en het was een maandag. Dat betekend dat iedereen nog op hun werk of school zit.

Woensdag waren ik en Nikola naar Robin gegaan om daar uitleg te krijgen over de insight cards. Deze gaan wij maken om zo de belangrijkste punten uit onze interviews te halen. Ook heeft Robin ons templates gegeven waarmee de insight cards kunnen maken. Het belangrijkste is om de relevante en unieke punten uit het interview te halen.
