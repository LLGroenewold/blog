---
title: Ideeën genereren (divergeren en convergeren )
date: 2018-06-21
---
 
## Introductie

- Vandaag heb ik mijn onderzoeksposter gepitcht en feedback op gekregen. Daarna heb ik ideeën moeten generen door middel van divergeren en convergeren

## Vandaag, donderdag 21 juni,

In de ochtend heb ik mijn onderzoeksposter gepitcht en hier heb ik feedback op gehad. Het feedback was positief. Alleen als verbeter punt heb ik gekregen dat ik de kleuren iets beter moet weer geven zodat het duidelijker leesbaar is. Ook moest het duidelijker zijn dat ik het over Barendrecht had. Dat was namelijk niet het geval.

Later heb ik ideeën hebben moeten generen. Dit heb ik gedaan door middel van creatieve technieken toe te passen. Ik heb een mindmap gemaakt en ook gebruik gemaakt van *"take your pick"*. Later heb ik mijn ideeën samengevoegd tot een concept en heb ik gebruikt gemaakt van *"Secret Vote"* om te convergeren. Het idee met de meeste stemmen werd dan mijn uiteindelijke keuzen.

Nu moet ik een concept poster gaan maken en deze ga ik morgen presenteren. 