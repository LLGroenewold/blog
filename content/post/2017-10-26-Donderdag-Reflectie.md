﻿---
title: Reflectie op de afgelopen periode
date: 2017-10-26
---

## Donderdag 26 oktober

**De dingen die wel goed zijn gegaan:**
Als iedereen aanwezig was ging de samenwerking gewoon prima en optimaal. Alles liep gewoon op rolletjes en werk kwam wel af. Het overleggen en samenwerken ging zeker prima en we waren het met alles wat werd voorgesteld door elkaar eens.


**De dingen die minder goed zijn gegaan:** 
- De afgelopen periode ging op zich wel goed alleen was het gewoon erg jammer dat er steeds teamleden uitvielen en niet goed communiceren over of hun wel kwamen of niet. Bijvoorbeeld bij Xander was het zo dat hij zei: "*Ik kom er zo aan*" en dat was dan rond 10/11 uur 's ochtends en dan kwam hij pas op 2 uur 's middags aan. Ook als mensen helemaal niet kwamen opdagen werd dit ook pas laat gezegd. Ook bij het vragen als er niks werd gezegd of mensen afwezig waren of waarom ze te laat waren werd er ook niks gezegd. Er was gewoon geen goede communicatie. En dan ook nog het feit dat als Xander zei: "*Jongens ik wil jullie graag aanwezig hebben om 12 uur*" dan komen wij als groepje wel op tijd maar hij zelf kwam nooit opdagen en heeft ook niks van zich laten horen. Dat is gewoon slecht.
