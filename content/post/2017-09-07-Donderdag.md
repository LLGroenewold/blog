﻿---
title: Vierde Project Dag
date: 2017-09-07
---

## Donderdag 7 september 2017
-	Deze dag hebben werkcollege gedaan. We hebben allemaal een ontwerpproces moeten maken. Deze gingen we van elkaar beoordelen en daarna hebben gezamenlijk nog een ontwerpproces gemaakt aan de hand van de individuele processen. Deze hebben we moeten presenteren.

-	Zelf heb ik nog een lijstje met concept ideeën gemaakt voor het spel. Deze gaan we vrijdag bespreken.


- Opdracht van de werk college:

![](Opdracht_Werkcollege.jpg)

- Lijst van de ideeën die ik heb verzonnen voor het spel:

![](Idee_lijst.JPG)

